#ifndef FEATURE_MANAGER_H
#define FEATURE_MANAGER_H

#include <list>
#include <algorithm>
#include <vector>
#include <numeric>
using namespace std;

#include <eigen3/Eigen/Dense>
using namespace Eigen;

#include <ros/console.h>
#include <ros/assert.h>

#include "parameters.h"

class FeaturePerFrame
{
  public:
    /**
     * @brief FeaturePerFrame
     * @param _point
     * @param td
     */
    FeaturePerFrame(const Eigen::Matrix<double, 7, 1> &_point, double td)
    {
        point.x() = _point(0);
        point.y() = _point(1);
        point.z() = _point(2);
        uv.x() = _point(3);
        uv.y() = _point(4);
        velocity.x() = _point(5); 
        velocity.y() = _point(6); 
        cur_td = td;
    }
    double cur_td;
    Vector3d point;
    Vector2d uv;
    Vector2d velocity;
    double z;
    bool is_used;
    double parallax;
    MatrixXd A;
    VectorXd b;
    double dep_gradient;
};

class FeaturePerId
{
  public:
    const int feature_id;
    int start_frame;
    vector<FeaturePerFrame> feature_per_frame;

    size_t used_num;
    bool is_outlier;
    bool is_margin;
    double estimated_depth;
    int solve_flag; // 0 haven't solve yet; 1 solve succ; 2 solve fail;

    Vector3d gt_p;

    /**
     * @brief FeaturePerId
     * @param _feature_id
     * @param _start_frame
     */
    FeaturePerId(int _feature_id, int _start_frame)
        : feature_id(_feature_id), start_frame(_start_frame),
          used_num(0), estimated_depth(-1.0), solve_flag(0)
    {
    }

    /**
     * @brief endFrame
     * @return
     */
    int endFrame();
};

class FeatureManager
{
  public:
    /**
     * @brief FeatureManager
     * @param _Rs
     */
    FeatureManager(Matrix3d _Rs[]);

    /**
     * @brief setRic
     * @param _ric
     */
    void setRic(Matrix3d _ric[]);

    /**
     * @brief clearState
     */
    void clearState();

    /**
     * @brief getFeatureCount
     * @return number of features
     */
    size_t getFeatureCount();

    /**
     * @brief addFeatureCheckParallax
     * @param frame_count
     * @param image
     * @param td
     * @return
     */
    bool addFeatureCheckParallax(int frame_count, const map<int, vector<pair<int, Eigen::Matrix<double, 7, 1>>>> &image, double td);
    /**
     * @brief debugShow
     */
    void debugShow();
    /**
     * @brief getCorresponding
     * @param frame_count_l
     * @param frame_count_r
     * @return
     */
    vector<pair<Vector3d, Vector3d>> getCorresponding(int frame_count_l, int frame_count_r);

    //void updateDepth(const VectorXd &x);
    /**
     * @brief setDepth
     * @param x
     */
    void setDepth(const VectorXd &x);
    /**
     * @brief removeFailures
     */
    void removeFailures();
    /**
     * @brief clearDepth
     * @param x
     */
    void clearDepth(const VectorXd &x);
    /**
     * @brief getDepthVector
     * @return
     */
    VectorXd getDepthVector();
    /**
     * @brief triangulate
     * @param Ps
     * @param tic
     * @param ric
     */
    void triangulate(Vector3d Ps[], Vector3d tic[], Matrix3d ric[]);
    /**
     * @brief removeBackShiftDepth
     * @param marg_R
     * @param marg_P
     * @param new_R
     * @param new_P
     */
    void removeBackShiftDepth(Eigen::Matrix3d marg_R, Eigen::Vector3d marg_P, Eigen::Matrix3d new_R, Eigen::Vector3d new_P);
    /**
     * @brief removeBack
     */
    void removeBack();
    /**
     * @brief removeFront
     * @param frame_count
     */
    void removeFront(int frame_count);
    /**
     * @brief removeOutlier
     */
    void removeOutlier();
    list<FeaturePerId> feature;
    int last_track_num;

  private:
    /**
     * @brief compensatedParallax2
     * @param it_per_id
     * @param frame_count
     * @return
     */
    double compensatedParallax2(const FeaturePerId &it_per_id, int frame_count);
    const Matrix3d *Rs;
    Matrix3d ric[NUM_OF_CAM];
};

#endif
